## coral-user 12 SP2A.220505.002 8353555 release-keys
- Manufacturer: google
- Platform: msmnile
- Codename: coral
- Brand: google
- Flavor: coral-user
- Release Version: 12
- Id: SP2A.220505.002
- Incremental: 8353555
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/coral/coral:12/SP2A.220505.002/8353555:user/release-keys
- OTA version: 
- Branch: coral-user-12-SP2A.220505.002-8353555-release-keys
- Repo: google_coral_dump_24571


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
